#include "child_process.h"
#include "threads/thread.h"
#include "stdio.h"
#include "list.h"
#include "threads/synch.h"
#include "threads/malloc.h"
#include "process.h"

//#define DEBUG

#ifdef DEBUG
#define DBG_PRINT_CP(...) printf("[CP_DEBUG]: "); printf(__VA_ARGS__); printf("\n")
#else
#define DBG_PRINT_CP(...)
#endif

static struct list children_list;

struct process_item * cp_alloc_and_init_item(tid_t parent_tid, tid_t tid)
{
	struct process_item * item  = malloc(sizeof(struct process_item));

	DBG_PRINT_CP("cp_alloc_and_and_init_item (0x%X, 0x%X);", parent_tid, tid);

	if(NULL == item)
	{
		return NULL;
	}

	lock_init(&item->lock);
	lock_acquire(&item->lock);
	item->parent_tid = parent_tid;
	item->tid = tid;
	item->status = STATUS_LOADING;
	item->ref_count = 2;
	item->return_code = 0;

	return item;
}

//
// Initialize the child process
//
void cp_init(void)
{
	DBG_PRINT_CP("Initializing CHILD PROCESS module");
	list_init(&children_list);
}

//
// adds a child to the list
//
void cp_add_child(struct process_item *child)
{
	if( NULL == child)
	{
		return;
	}

	DBG_PRINT_CP("cp_add_child (0x%X)", child);

	list_push_back(&children_list, &child->entry);
}

//
// removes a child of the list
//
void cp_remove_child(struct process_item *child)
{
	DBG_PRINT_CP( "cp_remove_child (0x%X)", child);
	list_remove(&child->entry);

	free(child);
}

//
// gets a child process
//
struct process_item * cp_get_child(tid_t parent_tid, tid_t child_tid)
{
	struct list_elem *entry = NULL;
	struct process_item *item = NULL;

	DBG_PRINT_CP("cp_get_child(0x%X, 0x%X)", parent_tid, child_tid);

	entry = list_front(&children_list);
	while(NULL != entry)
	{
		item = list_entry(entry, struct process_item, entry);

		if(NULL == item)
		{
			return NULL;
		}

		if(item->parent_tid == parent_tid && item->tid == child_tid)
		{
			return item;
		}

		entry = list_next(entry);
	}

	return NULL;
}

void cp_set_status(int status)
{
	struct list_elem *entry = NULL;
		struct process_item *item = NULL;

		entry = list_front(&children_list);
		while(NULL != entry)
		{
			item = list_entry(entry, struct process_item, entry);
			if(NULL == item)
			{
				break;
			}

			if(item->tid == thread_current()->tid)
			{
				item->status = status;
				break;
			}

			entry = list_next(entry);
		}
}


void syscall_exit ( int status)
{
	struct list_elem *entry = NULL;
	struct process_item *item = NULL;

	entry = list_front(&children_list);
	while(NULL != entry)
	{
		item = list_entry(entry, struct process_item, entry);
		if(NULL == item)
		{
			break;
		}

		if(item->tid == thread_current()->tid)
		{
			item->ref_count --;
			item->return_code = status;
			item->status = STATUS_DEAD;
			lock_release(&item->lock);
			if (0 == item->ref_count)
			{
				cp_remove_child(item);
			}
			break;
		}

		entry = list_next(entry);
	}

	printf ("%s: exit(%d)\n", thread_current()->name, status);
	thread_exit();
}

int syscall_wait(tid_t thread_tid)
{
	int status = -1;
	struct list_elem *entry = NULL;
	struct process_item *item = NULL;

	entry = list_front(&children_list);
	while(NULL != entry)
	{
		item = list_entry(entry, struct process_item, entry);
		if(NULL == item)
		{
			return (int)NULL;
		}

		if(item->tid == thread_tid)
		{
			item->ref_count --;
			lock_acquire(&item->lock);
		    status = item->return_code;
			lock_release(&item->lock);
			if (0 == item->ref_count)
			{
				cp_remove_child(item);
			}
			break;
		}

		printf("ENTRY = 0x%X\n", entry);

		if(entry == list_back(&children_list))
		{
			break;
		}
		entry = list_next(entry);
	}

	return (int)status;
}

int syscall_exec( char * command_line)
{
	struct process_item *item = NULL;
	int tid;

	tid = process_execute(command_line);
	item = cp_get_child(thread_current()->tid, tid);
	while (item->status == STATUS_LOADING)
	{
		//spin lock...
		barrier();
	}
	if(item->status == STATUS_FAILED)
	{
		return -1;
	}

	return item->tid;
}
