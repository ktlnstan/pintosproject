#ifndef _MEMORY_H_
#define _MEMORY_H_

#include <stdbool.h>

/* <RMP>
 * The functions described here should be used when reading/writing user mode memory.
 * In Kernel Mode user mode address must always be Probed and captured before accessing
 * Usually this is done in the context of an exception filter. Pintos does not have this
 * functionality, so we should try to protect against page faults using other methods
 *
 * Every time a user mode address must be use mem_probe or mem_probe_and_capture_buffer even if
 * this is not always efficient
 */

bool mem_is_user_address (void * address);

/* Convert a user mode virtual address to a real address
 * The function causes a kernel panic if it receives a kernel mode address.
 */
void * mem_convert_address( void * virtaddr);

/* Check if a user mode buffer is valid*/
bool mem_probe (void * virtaddr, unsigned int size);

/* captures a user mode buffer in the destination
 * returns true if the the function is successful
 */
bool mem_probe_and_capture_buffer( void * virtaddr, unsigned char* destination, unsigned int size );

/*
 * Read a string from a usermode address
 */
bool mem_probe_and_capture_string( void * virtaddr, unsigned char* destination, unsigned int max_size);

/*
 * Writes an ansi string to a user mode virtual address
 */
bool mem_probe_and_write_string( void * virtaddr, unsigned char* source, unsigned int max_size);

/* wtite a buffer to user mode
 * returns true if the the function is successful
 */
int mem_probe_and_write_buffer(void * virtaddr, unsigned char* source, unsigned int size);

#endif
