#include "memory.h"
#include "../threads/vaddr.h"
#include "../threads/thread.h"
#include "../threads/malloc.h"
#include "pagedir.h"
#include <stdbool.h>
#include <debug.h>

/* <RMP>
 * This file contains functions used to check user mode memory
 *
 */
/*<RMP> Check if a given address is in user mode.
 * Use this function before accessing any user mode address
 */
bool mem_is_user_address(void * address) {
	return address <= PHYS_BASE;
}

/* Convert a user mode virtual address to a real address
 * The function causes a kernel panic if it receives a kernel mode address.
 */
void *
mem_convert_address(void * virtaddr) {
	void * address = NULL;

	ASSERT(mem_is_user_address(virtaddr));

	address = pagedir_get_page(thread_current()->pagedir, virtaddr);

	return address;
}

/* Check if a user mode buffer is valid*/
bool
mem_probe(
		void * virtaddr,
		unsigned int size) {
	bool is_valid = true;
	unsigned char * address = NULL;

	// parse the whole buffer and check if it can be accessed
	for (address = virtaddr; address < (unsigned char *)virtaddr + size; address++) {
		if (!mem_is_user_address(address) || NULL == mem_convert_address(
				address)) {
			is_valid = false;
			break;
		}
	}

	return is_valid;
}

/* captures a user mode buffer in the destination
 * returns true if the the function is successful
 */
bool
mem_probe_and_capture_buffer(
		void * virtaddr,
		unsigned char* destination,
		unsigned int size) {
	bool is_success = true;
	unsigned int i;
	unsigned char * address = NULL;

	// parse the whole buffer and check if it can be accessed
	for (i = 0; i < size; i++) {
		if (!mem_is_user_address((unsigned char *) virtaddr + i)) {
			is_success = false;
			break;
		}

		address = mem_convert_address((unsigned char *) virtaddr + i);
		if (NULL == address) {
			is_success = false;
			break;
		}

		destination[i] = *(address);
	}

	return is_success;
}

/* captures a user ANSI string buffer in the destination
 * returns true if the the function is successful
 */
bool
mem_probe_and_capture_string(
		void * virtaddr,
		unsigned char* destination,
		unsigned int max_size)
{
	bool is_success = true;
	unsigned int i;
	unsigned char * address = NULL;

	// parse the whole buffer and check if it can be accessed
	for (i = 0; i < max_size; i++) {
		if (!mem_is_user_address((unsigned char *) virtaddr + i)) {
			is_success = false;
			break;
		}

		address = mem_convert_address((unsigned char *) virtaddr + i);
		if (NULL == address) {
			is_success = false;
			break;
		}

		destination[i] = *(address);
		if('\0' == destination[i])
		{
			break;
		}
	}

	return is_success;
}

/*
 * Writes an ansi string to a user mode virtual address
 */
bool
mem_probe_and_write_string(
		void * virtaddr,
		unsigned char* source,
		unsigned int max_size)
{
	bool is_success = true;
	unsigned int i;
	unsigned char * address = NULL;

	// parse the whole buffer and check if it can be accessed
	for (i = 0; i < max_size; i++) {
		if (!mem_is_user_address((unsigned char *) virtaddr + i)) {
			is_success = false;
			break;
		}

		address = mem_convert_address((unsigned char *) virtaddr + i);
		if (NULL == address) {
			is_success = false;
			break;
		}

		*(address) = source[i];
		if('\0' == source[i])
		{
			break;
		}
	}

	return is_success;
}

/* wtite a buffer to user mode
 * returns true if the the function is successful
 */
int
mem_probe_and_write_buffer(
		void * virtaddr,
		unsigned char* source,
		unsigned int size)
{
	bool is_success = true;
	unsigned int i;
	unsigned char * address = NULL;
	int result = 0;
	// parse the whole buffer and check if it can be accessed
	for (i = 0; i < size; i++) {
		if (!mem_is_user_address((unsigned char *) virtaddr + i)) {
			is_success = false;
			break;
		}

		address = mem_convert_address((unsigned char *) virtaddr + i);
		if (NULL == address) {
			is_success = false;
			break;
		}

		*(address) = source[i];
		result++;
	}
	if (is_success){
		return result;
	}
	if(result > 0 ){
		return result;
	}
	return -1;
}
