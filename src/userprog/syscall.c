#include "userprog/syscall.h"
#include "threads/palloc.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "userprog/memory.h"

static void syscall_handler(struct intr_frame *);

struct file_ids {
	int id;
	struct list_elem elem;
};

struct list file_ids_holder;


/*rzv*/

static struct lock file_lock;

static struct file_desc *get_file_descriptor(int fd) {

	struct list_elem *e;

	for (e = list_begin(&(thread_current()->file_descs)); e != list_end(
			&(thread_current()->file_descs)); e = list_next(e)) {
		struct file_desc *desc = list_entry(e , struct file_desc, elem);

		if (desc->id == fd) {
			return desc;
		}
	}

	return NULL;
}

void syscall_init(void) {
	intr_register_int(0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void syscall_handler(struct intr_frame *f) {
	int syscall_no = ((int*) f->esp)[0];
	int fd;
	char* file_name;
	void *buffer;
	unsigned pos, size;
	void *buffer;

	//printf ("system call!\n");
	//thread_exit ();

	switch (syscall_no) {
	case SYS_READ:
		fd = ((int*) f->esp)[1];
		buffer = (char*) ((int*) f->esp)[2];
		size = ((int*) f->esp)[3];

		f->eax = (uint32_t) read(fd, buffer, size);
		break;

	case SYS_SEEK:
		fd = ((int*) f->esp)[1];
		pos = ((unsigned*) f->esp)[2];

		seek(fd, pos);
		break;

	case SYS_CLOSE:

		break;

	case SYS_TELL:
		fd = ((int*) f->esp)[1];

		f->eax = (uint32_t) tell(fd);
		break;

	case SYS_CREATE:
			file_descriptor = ((char*) f->esp)[1];
		size = ((unsigned*) f->esp)[2];

		f_eax = (bool) create(file_descriptor, size);
		break;
	case SYS_REMOVE:
		file_descriptor = ((char*) f->esp)[1];

		f_eax = (bool) remove(file_descriptor);
		break;
	case SYS_OPEN:
		file_descriptor = ((char*) f->esp)[1];

		f_eax = (uint32_t) open(file_descriptor);
		break;
	case SYS_FILESIZE:
		fd = ((int*) f->esp)[1];

		f->eax = (uint32_t) filesize(fd);
		break;
	case SYS_WRITE:
		fd = ((int*) f->esp)[1];
		buffer = ((void*) f->esp)[2];
		size = ((int*) f->esp)[3];

		f->eax = (uint32_t) write(fd, buffer, size);

	}
}

void seek(int fd, unsigned pos) {
	struct file_desc *desc = get_file_descriptor(fd);

	if (desc && desc->file) {
		file_seek(desc->file, pos);
	}
}

int read(int fd, void *buffer, unsigned size) {
	unsigned i;

	//buffer verif
	if (buffer + size - 1 >= PHYS_BASE) {
		return -1;
	}

	//citire din consola
	if (fd == 0) {
		for (i = 0; i != size; ++i) {
			*(uint8_t *) (buffer + i) = input_getc();
		}
		return size;
	}

	//sau citire din fisier
	struct file_desc *desc = get_file_descriptor(fd);

	if (desc && desc->file) {
		return file_read(desc->file, buffer, size);//fread ret size ul
	}

	return -1;
}

int tell(int fd) {

	struct file_desc *desc = get_file_descriptor(fd);

	if (desc && desc->file) {
		return file_tell(desc->file);
	}

	return -1;
}

void close(int fd) {

	struct file_desc *desc = get_file_descriptor(fd);

	if (desc && desc->file) {
		file_close(desc->file);

		list_remove( &(desc->elem) );
		palloc_free_page(desc->file_page);
	}

}

bool create(const char *file, unsigned initial_size) {

	lock_acquire(&file_lock);
	bool ret = filesys_create(file, initial_size);
	lock_release(&file_lock);
	return ret;
}

bool remove(const char *file) {

	lock_acquire(&file_lock);
	bool ret = filesys_remove(file, initial_size);
	lock_release(&file_lock);
	return ret;
}

bool compare_list_id(const struct list_elem *a, const struct list_elem *b,
		void *aux) {
	struct file_ids *entry;
	int valueA = (entry = list_entry(a, struct file_ids, elem))->id;
	int valueB = (entry = list_entry(b, struct file_ids, elem))->id;
	return (valueA < valueB);
}

struct file_ids * create_Elem(int ident) {
	struct file_ids *element = (struct file_ids*) malloc(
			sizeof(struct file_ids));
	element->id = ident;
	return element;
}



int
allocate_fid(){

	struct list_elem *e;
	int last_used_value = 1;
	int fid = 0;

	if(file_ids_holder == NULL){
		list_init(&file_ids_holder);
	}

	for(e = list_begin(&(file_ids_holder)) ;
				e != list_end(&(file_ids_holder));
				e = list_next(e))
		{
			struct file_ids *file_id = list_entry(e , struct file_ids, elem);

			if(file_id->id == last_used_value + 1)
			{
				last_used_value = file_id->id;
			}
			else{
				break;
			}
		}
	fid = last_used_value + 1;
	if(last_used_value == 1){
		return 2;
	}

	list_insert_ordered(&file_ids_holder,&(create_Elem(fid)->elem),compare,NULL);
	return last_used_value + 1;

}

int
open(const char *file) {

	struct file *sys_file;
	struct file_desc *user_file;

	lock_acquire (&file_lock);
		sys_file = filesys_open (file, initial_size);
	lock_release (&file_lock);

	if (sys_file == NULL)
		return -1;

	user_file = (struct file_desc *) malloc (sizeof (struct file_desc));
	user_file->id = allocate_fid();
	user_file->file = sys_file;
	user_file->file_page = palloc_get_page(PAL_USER);

	return user_file->id;
}

int
filesize(int fd){
	int size = -1;

	struct file_desc *desc = get_file_descriptor(fd);

	if (f == NULL)
		return -1;

	lock_acquire (&file_lock);
	size = file_length (f->file);
	lock_release (&file_lock);

	return ret;
}


int write(int fd, const void *buffer, unsigned size) {
	unsigned i;

	int result = mem_probe_and_write_buffer(fd,buffer,size);

	/*if (mem_probe(buffer)) {
		//scriere pe monitor
		if (fd == 1) {
			for (i = 0; i != size; ++i) {
				*(uint8_t *) (buffer + i) = output_putc();
			}
			return size;
		}
		//sau scriere in fisier
		struct file_desc *desc = get_file_descriptor(fd);

		if (desc && desc->file) {
			return file_write(desc->file, buffer, size);//fwrite ret number of written bytes
		}
	}*/
	return result;
}

