#ifndef _CHILD_PROCESS_H_
#define _CHILD_PROCESS_H_

#include "threads/thread.h"
#include "threads/thread.h"
#include "list.h"
#include "threads/synch.h"
#include "process.h"

#define STATUS_LOADING  0
#define STATUS_READY	1
#define STATUS_DEAD     2
#define STATUS_FAILED   3

// a process item list. It is easier to implement this here, as it will not
// involve changing pintos structures
struct process_item
{
	struct 	list_elem entry;
	unsigned int	ref_count;
	tid_t 			parent_tid;
	tid_t 			tid;
	struct lock     lock;
	unsigned int	status;
	int             return_code;
};


struct process_item * cp_alloc_and_init_item(tid_t parent_tid, tid_t tid);
void cp_init(void);
void cp_add_child(struct process_item *child);
void cp_remove_child(struct process_item *child);
void cp_set_status(int status);
struct process_item * cp_get_child(tid_t parent_tid, tid_t child_tid);
void syscall_exit ( int status);
int syscall_wait(tid_t thread_tid);
int syscall_exec( char * command_line);

#endif
